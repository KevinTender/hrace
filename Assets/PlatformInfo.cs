﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformInfo : MonoBehaviour
{
    public ParticleSystem particleJump;
    private GameObject sceneControlerObj;
    private GameObject playerObj;
    public bool complete;
    public bool isMoveX;
    public bool isMoveY;
    private Vector3 blockStartPostion;
    public Vector3[] vectorState;
    public int blockState;
    public float moveSpeed;

    void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("player");
        sceneControlerObj = GameObject.FindGameObjectWithTag("SceneControler");
        blockStartPostion = gameObject.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (complete)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position - new Vector3(0, 5, 0), Time.deltaTime);
        }
        else
        {
            if (isMoveX)
            {
                if (transform.localPosition.x != blockStartPostion.x + vectorState[blockState].x)
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, blockStartPostion + vectorState[blockState], Time.deltaTime* moveSpeed);
                }
                else
                {
                    blockState++;
                    if (blockState == vectorState.Length)
                    {
                        blockState = 0;
                    }
                }
            }
            if (isMoveY)
            {
                if (transform.localPosition.y != blockStartPostion.y + vectorState[blockState].y)
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, blockStartPostion + vectorState[blockState], Time.deltaTime * moveSpeed);
                }
                else
                {
                    blockState++;
                    if (blockState == vectorState.Length)
                    {
                        blockState = 0;
                    }
                }
            }

        }
    }
    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "player")
        {
            transform.GetComponent<EdgeCollider2D>().isTrigger = true;
            transform.GetComponent<SpriteRenderer>().color -= new Color32(0, 0, 0, 150);
            particleJump.Play();
            CompleteBlock();
        }
    }
    void CompleteBlock()
    {
        complete = true;
        sceneControlerObj.GetComponent<SceneControler>().PlayerContactWithBlock();
        Destroy(gameObject.transform.parent.gameObject, 2);
    }
}
