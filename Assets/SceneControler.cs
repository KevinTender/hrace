﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneControler : MonoBehaviour
{
    public GameObject[] blockPrefab;
    public GameObject playerObj;
    public List<GameObject> spawnedBlocks;
    public int[] blocksAlpha;
    public int[] blocksPostion;
    public Color32[] blocksColor;
    public Text scoreText;
    public Text maxScoreText;
    public List<GameObject> listOfBlocks;

    private int maxScore;
    private int score;
    private Rigidbody2D playerRig;

    public Vector3 nextVector;

    public bool hard;

    void Start()
    {
        nextVector = new Vector3(0, -2, 8);
        playerRig = playerObj.GetComponent<Rigidbody2D>();
        maxScore = PlayerPrefs.GetInt("maxScore");
        maxScoreText.text = maxScore.ToString();
        SetBlocksParametrs();
    }
    void Update()
    {

    }
    void SetBlocksParametrs()
    {
        for(int i = 0; i < spawnedBlocks.Count; i++)
        {
            if(i<5)
            {
                Vector3 blockVector;
                blockVector = new Vector3(spawnedBlocks[i].transform.position.x, spawnedBlocks[i].transform.position.y, blocksPostion[i]);

                spawnedBlocks[i].GetComponent<BlockInfo>().SetBlockParametrs(blockVector,blocksColor[i]);
            }
            else
            {
                Vector3 blockVector;
                blockVector = new Vector3(spawnedBlocks[i].transform.position.x, spawnedBlocks[i].transform.position.y, blocksPostion[4]);

                spawnedBlocks[i].GetComponent<BlockInfo>().SetBlockParametrs(blockVector, blocksColor[4]);
            }
        }
    }
    public void SpawnBlock(int NumOfBlock)
    {
        Vector3 spawnPosition;
        spawnPosition = nextVector;

        nextVector = new Vector3(Random.Range(-2.5f, 2.5f), -2, 8);   

        Quaternion spawnRotation;
        if(nextVector.x > spawnPosition.x)
        {
            spawnRotation = Quaternion.Euler(0, 0, -10);
        }
        else
        {
            spawnRotation = Quaternion.Euler(0, 0, 10);
        }
        
        blockPrefab[NumOfBlock].transform.localScale = new Vector3(Random.Range(1, 6), 1, 1);
        spawnedBlocks.Add(Instantiate(blockPrefab[NumOfBlock], spawnPosition, spawnRotation));
        spawnedBlocks.RemoveAt(0);
        SetBlocksParametrs();

        spawnedBlocks[0].GetComponent<BlockInfo>().ThisNextPlatform(hard);

    }
    public void SpawnSegment(Vector3 StartLine, Vector3 EndLine, int BlocksInSegment,int NumOfBlock)
    {
        Vector3 startLine = StartLine;
        Vector3 endLine = EndLine;
        int blocksInSegment = BlocksInSegment;
        float stepX = (endLine.x - startLine.x) / blocksInSegment;
        float stepY = (endLine.y - startLine.y) / blocksInSegment;

        for (int i=0;i<blocksInSegment;i++)
        {
            Vector3 spawnPosition;
            spawnPosition = nextVector;

            nextVector = new Vector3(startLine.x+stepX*i, startLine.y+stepY*i, 8);

            Quaternion spawnRotation;
            if (nextVector.x > spawnPosition.x)
            {
                spawnRotation = Quaternion.Euler(0, 0, -10);
            }
            else
            {
                spawnRotation = Quaternion.Euler(0, 0, 10);
            }

            blockPrefab[NumOfBlock].transform.localScale = new Vector3(Random.Range(1, 6), 1, 1);
            spawnedBlocks.Add(Instantiate(blockPrefab[NumOfBlock], spawnPosition, spawnRotation));
       }
        spawnedBlocks.RemoveAt(0);
        SetBlocksParametrs();
        spawnedBlocks[0].GetComponent<BlockInfo>().ThisNextPlatform(hard);

    }
    public void PlayerContactWithBlock()
    {
        int r = Random.Range(0, 5);
        if(r ==0)
        {
            SpawnBlock(0);
        }
        if(r==1)
        {
            SpawnBlock(1);
        }
        if(r==2)
        {
            SpawnSegment(new Vector3(0,-2,8),new Vector3(2.75f,-2,8),3,2);
        }
        if(r==3)
        {
            SpawnSegment(new Vector3(-2.75f, -2.5f, 8), new Vector3(2.75f, 0, 8), 5, 3);
        }
        if(r==4)
        {
            SpawnBlock(4);
        }
        AddPoint();
        AddForce();
    }
    public void AddPoint()
    {
        score++;
        scoreText.text = score.ToString();

    }
    public void Lose()
    {
        if(score > maxScore)
        {
            PlayerPrefs.SetInt("maxScore",score);
            maxScoreText.text = maxScore.ToString();
        }
        SceneManager.LoadScene(0);

    }
    public void AddForce()
    {
        playerRig.velocity = Vector3.zero;
        playerObj.transform.position = playerObj.transform.position + new Vector3(0, 1, 0);
        //  playerRig.AddForce(((spawnedBlocks[0].transform.position - playerObj.transform.position).normalized*4 + new Vector3(0, 5, 0)) * 75);
        playerRig.AddForce(((spawnedBlocks[0].transform.position - playerObj.transform.position).normalized * 4 + new Vector3(0, 5, 0)) * 75);
    }
    public void Fall()
    {
        playerRig.velocity = Vector3.zero;
        playerRig.AddForce(new Vector3(0,-500,0));
    }
    public void StartGame()
    {
        playerRig.gravityScale = 1;
    }
}
