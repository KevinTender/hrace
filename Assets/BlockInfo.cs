﻿using UnityEngine;

public class BlockInfo : MonoBehaviour
{
    public GameObject platform;
    private Vector3 blockVector;
    private bool hard;

   
    public void ThisNextPlatform(bool Hard)
    {
        platform.GetComponent<EdgeCollider2D>().isTrigger = false;
        hard = Hard;
    }
    void Start()
    {
    
        blockVector = gameObject.transform.position;
    }
    public void SetBlockParametrs(Vector3 BlockVector, Color32 BlockColor)
    {
        platform.GetComponent<SpriteRenderer>().color = BlockColor;
        blockVector = BlockVector;
 
    }
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, blockVector, Time.deltaTime*5);
        if(hard)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale,new Vector3(0,0,0),Time.deltaTime*2);
        }
        
    }

}
