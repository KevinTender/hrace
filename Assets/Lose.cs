﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lose : MonoBehaviour
{
    public GameObject sceneControler;

    public void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag == "player")
        {
            sceneControler.GetComponent<SceneControler>().Lose();
        }
    }
}
